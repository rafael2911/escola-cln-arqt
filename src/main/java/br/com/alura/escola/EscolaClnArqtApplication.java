package br.com.alura.escola;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EscolaClnArqtApplication {

	public static void main(String[] args) {
		SpringApplication.run(EscolaClnArqtApplication.class, args);
	}

}
